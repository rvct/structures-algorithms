module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  parserOptions: {
    sourceType: "module",
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    tsconfigRootDir: __dirname,
    project: ["tsconfig.eslint.json"],
    ecmaVersion: 2020,
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: [
    "eslint:recommended",
    "eslint-config-import-sort",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:jest/recommended",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended",
  ],
  globals: {
    __dirname: true,
    process: true,
    module: true,
    require: true,
  },
  ignorePatterns: [
    // Specific glob pattern for parsing file under directories started with dot
    "!.*/**",
    "!.*",
    "!/*.js",
  ],
  rules: {
    "eol-last": [2, "always"],
    "no-multiple-empty-lines": [2, { max: 1, maxEOF: 0 }],
    "no-console": [2, { allow: ["warn", "error"] }],
    "no-redeclare": 2,
    "prettier/prettier": 2,
    "jest/expect-expect": 2,
    "no-unused-vars": [
      2,
      {
        vars: "all",
        args: "none",
        ignoreRestSiblings: false,
      },
    ],
    "no-unused-expressions": [
      2,
      {
        allowShortCircuit: true,
        allowTernary: true,
        allowTaggedTemplates: true,
      },
    ],
    "@typescript-eslint/no-redeclare": 2,
    "@typescript-eslint/ban-ts-comment": 1,
  },
};
