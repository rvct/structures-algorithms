# Structures and algorithms

A set of basic data structures and algorithms

## CLI Commands

```bash
# install dependencies
yarn

# run tests
yarn test
```
