module.exports = {
  types: [
    { value: "ci", name: "ci: Configure CI and working with scripts" },
    { value: "docs", name: "docs: Updating docs" },
    { value: "feat", name: "feat: Add new functionality" },
    { value: "fix", name: "fix: Fix bugs" },
    {
      value: "perf",
      name: "perf: Optimize perfomance",
    },
    {
      value: "refactor",
      name: "refactor: Edit the code without correcting bugs or adding new features",
    },
    { value: "revert", name: "revert: Rollback to previous commits" },
    {
      value: "style",
      name: "style: Update according to style guide (tabs, indents, points, commas, etc)",
    },
    { value: "test", name: "test: Add tests" },
  ],

  scopes: [
    { name: "components" },
    { name: "middlewares" },
    { name: "scripts" },
    { name: "no-release" },
  ],
  messages: {
    type: "What changes do you make?",
    scope: "Select the SCOPE, which you changed (optional):",
    customScope: "Set custom SCOPE (optional):",
    subject: "Write a short description:",
    body: "Meta data (ticket number). Example:(META: BETS-8000):",
    confirmCommit: "Are you satisfied with the resulting commit?",
  },
  skipQuestions: ["footer"],
  allowCustomScopes: true,
  subjectLimit: 72,
};
