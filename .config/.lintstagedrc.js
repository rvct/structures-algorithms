module.exports = {
  "*.{ts,tsx,js,jsx}": ["pnpm test:lint --fix", "pnpm test:prettier --write"],
  "*.{md,yml,yaml,json}": ["pnpm test:prettier --write"],
};
