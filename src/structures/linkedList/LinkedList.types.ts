export type LinkedListNode<T> = {
  data: T;
  prev: LinkedListNode<T>;
} | null;

export type LinkedList<T> = {
  push: (data: T) => LinkedListNode<T>;
  tail: () => LinkedListNode<T> | null;
  find: (data: T) => boolean;
  delete: (data: T) => T | null;
  [Symbol.iterator]: () => {
    current: LinkedListNode<T>;
    next: () => {
      done: boolean;
      value?: T;
    };
  };
};
