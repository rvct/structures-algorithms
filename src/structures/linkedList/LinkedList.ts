import type { LinkedList, LinkedListNode } from "./LinkedList.types";

export const linkedList = <T>(): LinkedList<T> => {
  let tail: LinkedListNode<T> = null;

  return {
    tail: () => tail,
    push: function (data: T) {
      tail = {
        data,
        prev: tail,
      };

      return tail;
    },
    find: function (data) {
      let current = tail;

      while (current) {
        if (JSON.stringify(current.data) === JSON.stringify(data)) return true;

        current = current.prev;
      }

      return false;
    },
    delete: function (data) {
      let deleted = null;

      if (!tail) return deleted;

      while (tail && JSON.stringify(tail.data) === JSON.stringify(data)) {
        deleted = tail.data;

        tail = tail.prev;
      }

      let current = tail;

      while (current && current.prev) {
        if (JSON.stringify(current.prev.data) === JSON.stringify(data)) {
          deleted = current.prev.data;

          current.prev = current.prev.prev;
        } else {
          current = current.prev;
        }
      }

      return deleted;
    },
    [Symbol.iterator]: () => ({
      current: tail,
      next: function () {
        const temp = this.current;

        if (!temp) {
          return {
            done: true,
          };
        }

        this.current = temp.prev;

        return {
          done: false,
          value: temp.data,
        };
      },
    }),
  };
};
