import { linkedList } from "./LinkedList";
import type { LinkedList } from "./LinkedList.types";

describe("Linked list", () => {
  const mockedData = [{ value: 1 }, { value: 2 }, { value: 3 }];
  let list: LinkedList<{ value: number }>;

  beforeEach(() => {
    list = linkedList();
    mockedData.forEach(list.push);
  });

  describe("should return correct tail", () => {
    it("if list isn't empty", () => {
      const [tail] = mockedData.slice(-1);

      expect(list.tail()?.data).toEqual(tail);
    });

    it("if list is empty", () => {
      list = linkedList();
      expect(list.tail()).toBeNull();
    });
  });

  it("should push element", () => {
    const data = { value: 4 };

    expect(list.push(data)?.data).toEqual(data);
    expect(list.tail()?.data).toEqual(data);
  });

  describe("should iterate correctly", () => {
    it("with for..of cycle", () => {
      const expectedList = [];

      for (const node of list) {
        expectedList.push(node);
      }

      expect(expectedList).toEqual([...mockedData].reverse());
    });

    it("with spread operator", () => {
      const expectedList = [...list];

      expect(expectedList).toEqual([...mockedData].reverse());
    });
  });

  it("should find element", () => {
    const data = mockedData[0];
    const notExisted = { value: 999 };

    expect(list.find(data)).toBe(true);
    expect(list.find(notExisted)).toBe(false);
  });

  describe("should delete element", () => {
    it("for empty list", () => {
      const data = mockedData[0];

      const list = linkedList();

      expect(list.delete(data)).toBeNull();
    });

    it("for tail match", () => {
      const data = { value: 5 };
      const expectedList = [...mockedData].reverse();

      list.push(data);

      expect(list.delete(data)).toEqual(data);
      expect([...list]).toEqual(expectedList);

      const notExistedData = { value: 999 };

      expect(list.delete(notExistedData)).toEqual(null);
      expect([...list]).toEqual(expectedList);
    });

    it("for middle match", () => {
      const data = { value: 5 };
      const [head, ...tail] = mockedData;

      list = linkedList();
      [head, data, ...tail].forEach(list.push);

      const expectedList = [...mockedData].reverse();

      expect(list.delete(data)).toEqual(data);
      expect([...list]).toEqual(expectedList);
    });
  });
});
