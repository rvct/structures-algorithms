import { doublyLinkedList } from "./DoublyLinkedList";
import type { DoublyLinkedList } from "./DoublyLinkedList.types";

describe("Doubly linked list", () => {
  const mockedData = [{ value: 1 }, { value: 2 }, { value: 3 }];
  let list: DoublyLinkedList<{ value: number }>;

  beforeEach(() => {
    list = doublyLinkedList();
    mockedData.forEach(list.push);
  });

  it("should push element", () => {
    const list = doublyLinkedList();

    expect(list.head()).toBeNull();
    expect(list.tail()).toBeNull();

    const data = { value: 1 };

    expect(list.push(data)?.data).toEqual(data);

    expect(list.head()?.data).toEqual(data);
    expect(list.tail()?.data).toEqual(data);

    const anotherData = { value: 2 };
    expect(list.push(anotherData)?.data).toEqual(anotherData);

    expect(list.head()?.data).toEqual(data);
    expect(list.tail()?.data).toEqual(anotherData);
  });

  describe("should iterate correctly", () => {
    it("with for..of cycle", () => {
      const expectedList = [];

      for (const node of list) {
        expectedList.push(node);
      }

      expect(expectedList).toEqual(mockedData.reverse());
    });

    it("with spread operator", () => {
      const expectedList = [...list];

      expect(expectedList).toEqual(mockedData.reverse());
    });
  });
});
