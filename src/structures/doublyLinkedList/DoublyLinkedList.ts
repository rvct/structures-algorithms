import type {
  DoublyLinkedList,
  DoublyLinkedListNode,
} from "./DoublyLinkedList.types";

export const doublyLinkedList = <T>(): DoublyLinkedList<T> => {
  let tail: DoublyLinkedListNode<T> = null;
  let head: DoublyLinkedListNode<T> = null;

  return {
    tail: () => tail,
    head: () => head,
    /**
     * Doubly linked list push behaviour
     *
     * initial state
     * null -- null
     * after first push
     * null -- {data, prev, next} (tail/head)-- null
     * after next push
     * null -- {data, prev, next} (head) -- {data, prev, next} (tail) -- null
     *
     */
    push: (data: T) => {
      head = head
        ? head
        : {
            data,
            prev: null,
            next: tail,
          };

      tail = {
        data,
        prev: tail,
        next: null,
      };

      return tail;
    },
    [Symbol.iterator]: () => ({
      current: tail,
      next: function () {
        const temp = this.current;

        if (!temp) {
          return {
            done: true,
          };
        }

        this.current = temp.prev;

        return {
          done: false,
          value: temp.data,
        };
      },
    }),
  };
};
