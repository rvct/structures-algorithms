export type DoublyLinkedListNode<T> = {
  data: T;
  prev: DoublyLinkedListNode<T>;
  next: DoublyLinkedListNode<T>;
} | null;

export type DoublyLinkedList<T> = {
  push: (data: T) => DoublyLinkedListNode<T>;
  head: () => DoublyLinkedListNode<T> | null;
  tail: () => DoublyLinkedListNode<T> | null;
  find?: (data: T) => boolean;
  delete?: (data: T) => T | null;
  [Symbol.iterator]: () => {
    current: DoublyLinkedListNode<T>;
    next: () => {
      done: boolean;
      value?: T;
    };
  };
};
