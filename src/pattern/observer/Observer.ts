import type {
  Observable as TObservable,
  Observer as TObserver,
} from "./Observer.types";

export const Observable: TObservable = {
  observers: [],
  subscribe: function (observer: TObserver) {
    const idx = this.observers.indexOf(observer);

    if (idx === -1) this.observers.push(observer);
  },
  unsubscribe: function (observer: TObserver) {
    const idx = this.observers.indexOf(observer);

    if (idx !== -1) this.observers.splice(idx, 1);
  },
  unsubscribeAll: function () {
    this.observers = [];
  },
  notify(data) {
    this.observers.forEach((observer) => observer.update(data));
  },
};
