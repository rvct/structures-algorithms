export type Observable = {
  observers: Observer[];
  subscribe: (observer: Observer) => void;
  unsubscribe: (observer: Observer) => void;
  unsubscribeAll: () => void;
  notify: (data: unknown) => void;
};

export type Observer = {
  update: (data: any) => void;
};
