import { Observable } from "./Observer";
import type { Observable as TObservable, Observer } from "./Observer.types";

describe("Obsverver pattern", () => {
  const list = Array.from({ length: 10 }).map((_, idx) => idx + 1);
  let processed: number[] = [];

  const observerEveryOdd: Observer = {
    update: jest.fn().mockImplementation(([data]: [number]) => {
      return data % 2 ? [data] : [];
    }),
  };

  const observerEveryEven: Observer = {
    update: jest.fn().mockImplementation(([data]: [number]) => {
      return data % 2 ? [] : [data];
    }),
  };

  let tickets = {} as TObservable & {
    state: number[];
    sell: () => void;
  };

  beforeEach(() => {
    tickets = Object.assign<
      TObservable,
      {
        state: number[];
        sell: () => void;
      }
    >(Object.create(Observable), {
      state: list,
      sell: function () {
        processed = this.state.splice(0, 1);
        // @ts-ignore:next-line
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        this.notify(processed);
      },
    });
  });

  afterEach(() => {
    tickets.unsubscribeAll();
  });

  test("should subscribe/unsubcribe observer", () => {
    tickets.subscribe(observerEveryOdd);
    expect(tickets.observers.length).toEqual(1);

    tickets.unsubscribe(observerEveryOdd);
    expect(tickets.observers.length).toEqual(0);
  });

  test("should subscribe/unsubcribe unique observer", () => {
    tickets.subscribe(observerEveryOdd);
    expect(tickets.observers.length).toEqual(1);

    tickets.subscribe(observerEveryOdd);
    expect(tickets.observers.length).toEqual(1);

    tickets.subscribe(observerEveryEven);
    expect(tickets.observers.length).toEqual(2);

    tickets.unsubscribe(observerEveryOdd);
    expect(tickets.observers.length).toEqual(1);
  });

  test("should unsubcribe all observers", () => {
    tickets.subscribe(observerEveryOdd);
    tickets.subscribe(observerEveryEven);
    expect(tickets.observers.length).toEqual(2);

    tickets.unsubscribeAll();
    expect(tickets.observers.length).toEqual(0);
  });

  test("should notify observers", () => {
    tickets.subscribe(observerEveryOdd);
    tickets.subscribe(observerEveryEven);

    tickets.sell();
    expect(observerEveryEven.update).toBeCalledWith([1]);

    tickets.sell();
    expect(observerEveryOdd.update).toBeCalledWith([2]);
  });
});
