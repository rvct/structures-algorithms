// @ts-nocheck
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
export function Singleton() {
  if (Singleton._instance) {
    return Singleton._instance;
  }

  if (!(this instanceof Singleton)) {
    return new Singleton();
  }

  Singleton._instance = this;

  Singleton.getInstance = function () {
    return Singleton._instance || new Singleton();
  };
}
