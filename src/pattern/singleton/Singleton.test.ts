// @ts-nocheck
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Singleton } from "./Singleton";

describe("Singleton pattern", () => {
  describe("for exsiting instance of Singleton", () => {
    const singleton = new Singleton();

    test("should return same object with new keyword", () => {
      const obj = new Singleton();

      expect(obj).toEqual(singleton);
    });

    test("should return same object without new keyword", () => {
      delete Singleton._instance;

      const obj = Singleton();

      expect(obj).toEqual(singleton);
    });

    test("should return same for instanse metod", () => {
      const obj = Singleton.getInstance();

      expect(obj).toEqual(singleton);
    });
  });

  describe("for abscent instance of Singleton", () => {
    beforeEach(() => {
      delete Singleton._instance;
    });

    test("should return instanse with static method", () => {
      expect(Singleton._instanse).toEqual(undefined);

      const obj = Singleton.getInstance();

      expect(obj).toBeInstanceOf(Singleton);
    });
  });
});
